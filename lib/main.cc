#include <node.h>
#include "thread.h"

void thread_factory(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::HandleScope scope(isolate);

	Thread::New(args);
}

void init(v8::Handle<v8::Object> exports, v8::Handle<v8::Object> module)
{
	Thread::Init();
	
	NODE_SET_METHOD(module, "exports", thread_factory);
}

NODE_MODULE(addon, init)