#ifndef __THREAD_H_
#define __THREAD_H_

#include <node.h>
#include <node_object_wrap.h>
#include <string>
#include <vector>
#include <mutex>
#include <thread>
#include <condition_variable>

class Thread : public node::ObjectWrap
{
public:
	static void Init();
	static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
		
	static v8::Persistent<v8::Function> constructor;
	static std::vector<std::thread*> threadPool;
	static std::mutex mtx;

private:
	explicit Thread();
	~Thread();

	static void Start(const v8::FunctionCallbackInfo<v8::Value>& args);
	static void SetThread(const v8::FunctionCallbackInfo<v8::Value>& args);
	static void Join(const v8::FunctionCallbackInfo<v8::Value>& args);
	
	std::string* threadFunction = NULL;
	v8::Isolate* isolate = NULL;
	
	std::mutex threadMutex;
	std::condition_variable condv;
	bool threadFinished = false;

	int threadIndex = -1;

	void ThreadWorker();
};

#endif