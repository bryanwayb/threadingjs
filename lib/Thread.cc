#include <node.h>
#include <string>
#include <iostream>
#include <vector>
#include <mutex>
#include <thread>
#include <condition_variable>
#include "thread.h"

v8::Persistent<v8::Function> Thread::constructor;
std::vector<std::thread*> Thread::threadPool;
std::mutex Thread::mtx;

void Thread::Init()
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::Locker lock(isolate);
	v8::HandleScope scope(isolate);

	v8::Local<v8::FunctionTemplate> funcTemp = v8::FunctionTemplate::New(isolate, New);
	
	funcTemp->SetClassName(v8::String::NewFromUtf8(isolate, "thread"));
	funcTemp->InstanceTemplate()->SetInternalFieldCount(3);
	
	NODE_SET_PROTOTYPE_METHOD(funcTemp, "start", Start);
	NODE_SET_PROTOTYPE_METHOD(funcTemp, "setThread", SetThread);
	NODE_SET_PROTOTYPE_METHOD(funcTemp, "join", Join);
	
	constructor.Reset(isolate, funcTemp->GetFunction());
}

Thread::Thread() { }
Thread::~Thread()
{
	if(this->isolate)
	{
		this->isolate->Dispose();
	}
}

void Thread::New(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::Locker lock(isolate);
	v8::HandleScope scope(isolate);
	
	if(args.IsConstructCall())
	{
		Thread* t = new Thread();
		t->Wrap(args.This());
		
		if(args.Length() >= 1)
		{
			Thread::SetThread(args);
		}

		args.GetReturnValue().Set(args.This());
	}
	else
	{
		int argc = args.Length();
		v8::Local<v8::Value>* argv = new v8::Local<v8::Value>[argc];
		
		for(int i = 0; i < argc; i++)
		{
			argv[i] = args[i];
		}
		
		args.GetReturnValue().Set(v8::Local<v8::Function>::New(isolate, constructor)->NewInstance(argc, argv));
	}
}

void Thread::SetThread(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::Locker lock(isolate);
	v8::HandleScope scope(isolate);
	
	if(args.Length() < 1 || !args[0]->IsFunction())
	{
		isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Requires that a function be passed")));
		return;
	}
	
	Thread* t = node::ObjectWrap::Unwrap<Thread>(args.Holder());
	
	v8::String::Utf8Value utf8(v8::Local<v8::StringObject>::Cast(args[0]));
		
	t->threadFunction = new std::string("(" + std::string(*utf8) + ")();");
	
	args.GetReturnValue().Set(v8::Boolean::New(isolate, true));
}

void Thread::Start(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::Locker lock(isolate);
	v8::HandleScope scope(isolate);
	
	Thread* t = node::ObjectWrap::Unwrap<Thread>(args.Holder());
	
	if(!t->threadFunction)
	{
		isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "No thread function has been set")));
		return;
	}
	
	Thread::mtx.lock();
	
	t->threadIndex = Thread::threadPool.size();
	Thread::threadPool.resize(t->threadIndex + 1);
	
	std::thread worker(&Thread::ThreadWorker, t);
	Thread::threadPool[t->threadIndex] = &worker;
	worker.detach();
	
	Thread::mtx.unlock();
	
	args.GetReturnValue().Set(v8::Boolean::New(isolate, true));
}

void Thread::Join(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::Locker lock(isolate);
	v8::HandleScope scope(isolate);
	
	Thread* t = node::ObjectWrap::Unwrap<Thread>(args.Holder());
	
	if(!t->threadFinished)
	{
		std::unique_lock<std::mutex> mtxLock(t->threadMutex);
	    t->condv.wait(mtxLock, [&]
		{
			return t->threadFinished;
		});
	}
	
	args.GetReturnValue().Set(v8::Boolean::New(isolate, t->threadFinished));
}

void Thread::ThreadWorker()
{
	if(!this->isolate)
	{
		this->isolate = v8::Isolate::New();
	}
	
	this->isolate->Enter();
	v8::Locker lock(this->isolate);
	v8::Isolate::Scope isolate_scope(this->isolate);
	v8::HandleScope handle_scope(this->isolate);
	
	v8::Local<v8::Context> context = v8::Context::New(this->isolate);
	v8::Context::Scope context_scope(context);
	
	v8::Local<v8::Script> script = v8::Script::Compile(v8::String::NewFromUtf8(isolate, (*this->threadFunction).c_str()));
	
	v8::Local<v8::Value> result = script->Run();
	
	std::cout << "Script returned: " << *(v8::String::Utf8Value(result)) << "\n";
	
	std::lock_guard<std::mutex> mtxLock(this->threadMutex);
	this->threadFinished = true;
	this->condv.notify_one();
	
	Thread::mtx.lock();
	int newSize = Thread::threadPool.size() - 1;
	for(int i = this->threadIndex; i < newSize; i++)
	{
		Thread::threadPool[i] = Thread::threadPool[i + 1];
	}
	Thread::threadPool.resize(newSize);
	Thread::mtx.unlock();
	
	v8::Unlocker unlock(this->isolate);
	this->isolate->Exit();
}