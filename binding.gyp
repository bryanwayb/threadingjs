{
  "targets": [
    {
      "target_name": "threading",
      "sources": [
        "lib/main.cc",
        "lib/thread.cc",
        "lib/thread.h"
      ],
      "conditions": [
        [ "OS == 'mac'", {
          "MACOSX_DEPLOYMENT_TARGET": "10.9",
          "xcode_settings": {
            "OTHER_CPLUSPLUSFLAGS" : [
              "-std=c++11"
            ]
          }
        }],
        [ "OS == 'linux'", {
          "cflags": [
            "-std=c++11"
          ]
        }]
      ],
      "include_dirs": [
        "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/v1"
      ]
    }
  ]
}
