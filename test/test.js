var threading = require('../build/Release/threading');

function loopThreadCallback() {
	return 'reused thread callback';
}

for(var i = 0; i < 10; i++) {
	threading(loopThreadCallback).start();
}

var test = threading(function() { return 'testing'; });
var anotherTest = threading();
anotherTest.setThread(function() { return 'from another thread callback'; });

test.start();
test.start();
anotherTest.start();

test.join();